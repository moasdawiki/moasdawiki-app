# Changelog

## 3.7.1.0 (versionCode 42, 2024-01-01)

- Update to moasdawiki-server 3.7.1
- Upgrade to Gradle 8.5
- Update copyright year

## 3.6.3.2 (versionCode 41, 2023-12-12)

- Upgrade library dependencies

## 3.6.3.1 (versionCode 40, 2023-11-15)

- Fix Gradle build issue in fdroid build

## 3.6.3.0 (versionCode 39, 2023-11-11)

- Limit calendar import to 100 events as Android has a global limit of 500 events
- Retry failed server sync requests
- Update to moasdawiki-server 3.6.3
- Upgrade library dependencies
- Upgrade to Android API 34 (Android 14)

## 3.6.2.1 (versionCode 38, 2023-08-07)

- Fix Gradle build issue

## 3.6.2.0 (versionCode 37, 2023-08-05)

- Update to moasdawiki-server 3.6.2
- Upgrade library dependencies

## 3.6.1.0 (versionCode 36, 2023-04-29)

- Update to moasdawiki-server 3.6.1
- Upgrade library dependencies

## 3.6.0.0 (versionCode 35, 2023-01-01)

- Update to moasdawiki-server 3.6.0
- Upgrade library dependencies
- Update copyright year

## 3.5.1.0 (versionCode 34, 2022-11-12)

- Update to moasdawiki-server 3.5.1
- Upgrade library dependencies

## 3.5.0.1 (versionCode 33, 2022-10-15)

- Add app icon to fastlane folder

## 3.5.0.0 (versionCode 32, 2022-10-08)
 
- Update to moasdawiki-server 3.5.0
- Upgrade library dependencies

## 3.4.5.0 (versionCode 31, 2022-09-05)

- Update to moasdawiki-server 3.4.5
- Upgrade library dependencies

## 3.4.4.0 (versionCode 30, 2022-04-14)

- Update to moasdawiki-server 3.4.4
- Upgrade library dependencies

## 3.2.1.0 (versionCode 29, 2021-12-29)

- Update Gradle version in moasdawiki-server and in app to fix build issue
- Update to moasdawiki-server 3.2.1

## 3.1.0.0 (versionCode 28, 2021-12-25)

- Small layout optimizations
- Update to moasdawiki-server 3.1.0
- Support for Android 12
- Upgrade library dependencies
- Update copyright year

## 2.6.1.0 (versionCode 27, 2021-06-09)

- Remove double back key press to close the app
- Update to moasdawiki-server 2.6.1
- Upgrade library dependencies

## 2.6.0.0 (versionCode 26, 2021-05-13)

- Make GPL-3.0-only licensing more clear
- Update to moasdawiki-server 2.6.0

## 2.5.2.1 (versionCode 25, 2021-05-08)

- Update documentation
- Update library dependencies

## 2.5.2.0 (versionCode 24, 2021-02-28)

- Find also words that start with the search string
- Update to moasdawiki-server 2.5.2

## 2.5.1.0 (versionCode 23, 2021-01-31)

- Enhance search index to full words
- Introduce search ignore list
- Add support for SVG images
- Update to moasdawiki-server 2.5.1

## 2.4.4.0 (versionCode 22, 2021-01-23)

- Speed up search
- Update to moasdawiki-server 2.4.4

## 2.4.3.0 (versionCode 21, 2021-01-04)

- Update descriptions for F-Droid
- Update copyright year
- Bugfix: Disable wiki editor handler
- Update to moasdawiki-server 2.4.3

## 2.4.2.0 (versionCode 20, 2020-12-28)

- Update to moasdawiki-server 2.4.2
- Upgrade library dependencies
- Refactoring: Replace deprecated AsyncTask

## 2.3.3.0 (versionCode 19, 2020-10-11)

- Bugfix: Show start page on app startup
- Bugfix: For searches only use the search index cache
- Update to moasdawiki-server 2.3.3

## 2.3.1.0 (versionCode 18, 2020-09-13)

- Speed up full text search by a search index
- Bugfix: Don't go back to start page after App restore
- Update to moasdawiki-server 2.3.1

## 2.2.1.1 (versionCode 17, 2020-09-08)

- Add German fastlane metadata for F-Droid repository
- Update library dependencies

## 2.2.1.0 (versionCode 16, 2020-03-15)

- Add fastlane metadata for F-Droid repository

## 2.2.0.0 (versionCode 15, 2020-02-08)

- Speed up full text search by searching only in the page titles in the first step

## 2.1.0.0 (versionCode 14, 2020-01-11)

- Renewed the user interface
- Birthdays and events can be provided as an Android calendar
- A lot of refactoring, updated to androidx packages
- Added `@Nullable` and `@NotNull` annotations, fixed potential bugs
- Removed UDP broadcast support as it didn't really work for the app
- Updated copyright
- Published sources on GitLab

## 2.0.9.2 (versionCode 13, 2019-01-03)

- HTML layout changed to border-box
- targetSdkVersion updated due to recent Google requirements
- Do not use an internal TCP port any more. Thus, the app requires no
  Internet permission for normal usage, it is only necessary for synchronisation.
- Bugfix: Fixed broken search
- Bugfix: Page titles with space or special characters couldn't be shown

## 2.0.8 (versionCode 9, 2018-02-05)

- App can be closed by pressing the back button twice

## 2.0.7 (versionCode 7, 2017-05-28)

- Bugfix: Editor icons got visible on scrolling

## 2.0.6 (versionCode 6, 2016-11-28)

- Bugfix: Edit symbol was still available and active

## 2.0.5 (versionCode 5, 2016-11-22)

- Initial App
