# MoasdaWiki App

## Description

MoasdaWiki App is a privacy-friendly frontend for the MoasdaWiki Server
knowledge management tool. It mirrors the Wiki content on your mobile device.

For MoasdaWiki documentation see https://moasdawiki.net/.

## Download

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
    alt="Get it on F-Droid"
    height="80">](https://f-droid.org/packages/net.moasdawiki.app)
or [Google Play](https://play.google.com/store/apps/details?id=net.moasdawiki.app)

## Features

- Synchronizes the content from your MoasdaWiki Server instance.
- Fast full text search.
- Calendar integration: Shows birthdays and events in the mobile calendar.
- Data privacy by design: Directly connects to your server instance in your
  private network. No trackers, never establishes a cloud connection.
- Wiki content cannot be modified within the app as it is no fun to type Wiki
  syntax on a mobile device, changes have to be done via the MoasdaWiki Server.

## Screenshots

<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/1.png" width="250" height="500" />
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/2.png" width="250" height="500" />
<img src="fastlane/metadata/android/en-US/images/phoneScreenshots/4.png" width="250" height="500" />

## Synchronize content with a MoasdaWiki Server

1. Download MoasdaWiki Server from https://gitlab.com/moasdawiki/moasdawiki-server.
2. Set up a MoasdaWiki Server instance in your LAN.
3. Enable LAN access to the server: Edit the repository file `config.txt` and change the setting `authentication.onlylocalhost = false`. Restart the server afterwards.
4. Install the MoasdaWiki App.
5. In the app you can see a hint that it has to be configured first. Press on that hint.
6. Press on "Host name" and enter the host name or IP address of the server instance, e.g. `192.168.1.101`. Press OK.
7. In the status section below you should see "Needs authorization at server". Otherwise check host name and port again.
8. On server side open the Wiki page in a browser, click on "Help" and "Synchronization".
9. You can see a list of devices and synchronization sessions. Check the device name and click on "Grant".
10. Back in the app press the back button (&larr;) on the upper left corner to get back to the main dialog. Now you can see a hint that the app has to be synchronized. Press on that hint.
11. Now you should have all the server content also in the app and you can see the "Home-App" wiki page.

## Support

If you have questions or any problems you can contact me via [support@moasdawiki.net]().

## License

MoasdaWiki server is licensed under GPL-3.0-only &ndash; see the
[LICENSE](LICENSE) file for details.

Copyright (C) Herbert Reiter
