/*
 * MoasdaWiki App
 * Copyright (C) 2008 - 2025 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3 as published
 * by the Free Software Foundation (GPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/gpl-3.0.html>.
 */

package net.moasdawiki.app;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Stub content provider, necessary for sync adapter.
 */
public class CalendarContentProvider extends ContentProvider {

    @Override
    public boolean onCreate() {
        return true;
    }

    @Nullable
    @Override
    public String getType(@NotNull Uri uri) {
        // Return no type for MIME type
        return null;
    }

    @Nullable
    @Override
    public Cursor query(@NotNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        // query() always returns no results
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NotNull Uri uri, @Nullable ContentValues contentValues) {
        // Provider doesn't support changes from outside
        return null;
    }

    @Override
    public int delete(@NotNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        // Provider doesn't support changes from outside
        return 0;
    }

    @Override
    public int update(@NotNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        // Provider doesn't support changes from outside
        return 0;
    }
}
