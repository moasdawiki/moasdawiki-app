/*
 * MoasdaWiki App
 * Copyright (C) 2008 - 2024 Herbert Reiter (herbert@moasdawiki.net)
 *
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3 as published
 * by the Free Software Foundation (GPL-3.0-only).
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/gpl-3.0.html>.
 */

package net.moasdawiki.app;

import net.moasdawiki.base.Logger;
import net.moasdawiki.base.Settings;
import net.moasdawiki.service.repository.RepositoryService;

import org.jetbrains.annotations.NotNull;

public class AndroidSettings extends Settings {

    public AndroidSettings(@NotNull Logger logger, @NotNull RepositoryService repositoryService, @NotNull String configFileName) {
        super(logger, repositoryService, configFileName);
    }

    @Override
    @NotNull
    public String getVersion() {
        return BuildConfig.VERSION_NAME;
    }
}
